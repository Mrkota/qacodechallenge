import java.net.MalformedURLException;
import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;

public class MyFirstTest {
	
	
	IOSDriver driver;
	 public static final String EMAIL = "markovurnek0@gmail.com";
	 public static final String PASSWORD= "marko123!";
	
	@Before
	public void setup() throws MalformedURLException{
		
	DesiredCapabilities capa = new DesiredCapabilities();
	capa.setCapability("deviceName", "iPhone Simulator");
	capa.setCapability("app", "/Users/markovurnek/Library/Developer/Xcode/DerivedData/GlookoFDA-hemftewguwysnjhcxsefayytbljn/Build/Products/Debug-iphonesimulator/Glooko.app");
	capa.setCapability("automationName", "XCUITest");
		
	 driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capa);
		
	}
	
	
	
	@After
	public void teardown(){
		
		driver.quit();
		
		}

	
	@Test
	public void firstTest() throws InterruptedException{
		
	  	TouchAction tapLoginButton = new TouchAction(driver);
	  	TouchAction tapEmailField = new TouchAction(driver);
	 	TouchAction tapLogin = new TouchAction(driver);
		TouchAction tapOkay = new TouchAction(driver);
		TouchAction tapReminders = new TouchAction(driver);
		TouchAction plusButton = new TouchAction(driver);
		TouchAction enterInsulin = new TouchAction(driver);
		TouchAction unitsOfInsulin = new TouchAction(driver);
		TouchAction saveButton = new TouchAction(driver);
		TouchAction timeOfDay = new TouchAction(driver);
		TouchAction setTime = new TouchAction(driver);
		TouchAction timeDoneButton = new TouchAction(driver);
		TouchAction insulinDoneButton = new TouchAction(driver);
		
//Tap on "Allow" in notification pop-up		
   new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(By.id("Allow")));
   driver.findElementByAccessibilityId("Allow").click();
  Thread.sleep(3000);
  

//tap the Login button 
	tapLoginButton.tap(200, 612).perform();
    Thread.sleep(3000);
    
 	
	
	//tap on Email field and write in it, then switch to Password field and write in it
    tapEmailField.tap(187, 250).perform();
	   driver.getKeyboard().pressKey(EMAIL);
	   Thread.sleep(2000);
	   driver.hideKeyboard();
	   driver.getKeyboard().pressKey(PASSWORD);
	   Thread.sleep(2000);
	   
	   //Tap Login button
	   tapLogin.tap(187, 333).perform();
	   Thread.sleep(20000);
	 
	   //Tap on "Okay" button inside a pop-up window
	   tapOkay.tap(187, 380).perform();
	
	//Click on "More" button  
    new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.name("More")));   
	driver.findElementByAccessibilityId("More").click();
   
	//Click on "Reminders" button
	tapReminders.tap(50, 141).perform();
	Thread.sleep(5000);
	
	//Tap on the "+" button on the top right
	plusButton.tap(370, 10).perform();
	Thread.sleep(2000);
	

	//Tap on "Take Insulin" button
	  new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(By.name("Take Insulin")));   
		driver.findElementByAccessibilityId("Take Insulin").click();
		Thread.sleep(2000);
		
	//Enter type of insulin
		enterInsulin.tap(52, 357).perform();
		
	//Pick a type of insulin (Apidra)
		 new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(By.name("Apidra")));   
		 driver.findElementByAccessibilityId("Apidra").click();
		 Thread.sleep(2000);
		 
	//Tap and enter 2 units of insulin
		 unitsOfInsulin.tap(290, 357).perform();
		 driver.getKeyboard().pressKey("2");
		
		 Thread.sleep(2000);
		 
	 //Tap on "Done" button after insulin ad units are selected
		insulinDoneButton.tap(370,422).perform();
		
		 //Tap on "Time of day" button
		 timeOfDay.tap(10,120).perform();
		 Thread.sleep(2000);

		 //Set time
		 setTime.tap(187, 635).perform();
		 Thread.sleep(2000);
		 
		 //Tap on "Done" button after time is set
		 timeDoneButton.tap(370,422).perform();
	
	 //Tap on the "Save" button
		 saveButton.tap(370, 40).perform();
		 Thread.sleep(2000);
		 
		 //Tap on "I took it" button
		 new WebDriverWait(driver,125).until(ExpectedConditions.visibilityOfElementLocated(By.name("I took it")));		 
		 driver.findElementByAccessibilityId("I took it").click();
		 Thread.sleep(2000);
		 
		 //Tap on Graphs button
		 driver.findElementByAccessibilityId("Graphs").click();
		 Thread.sleep(2000);
		 
		 //Find the 2 entered units in history
		driver.findElementByAccessibilityId("2 units");
		 Thread.sleep(2000);
		 
	    /*	 
		//Tap on Profile button and then log out
		 driver.findElementByAccessibilityId("Profile").click();
		 new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOfElementLocated(By.name("LOG OUT")));
		 driver.findElementByAccessibilityId("LOG OUT").click();
		 */
		 
}
	
	@Test
	public void secondTest() throws InterruptedException{
		
		TouchAction tapLoginButton = new TouchAction(driver);
	  	TouchAction tapEmailField = new TouchAction(driver);
	 	TouchAction tapLogin = new TouchAction(driver);
		TouchAction tapOkay = new TouchAction(driver);
		TouchAction tapMeter = new TouchAction(driver);
		TouchAction tapAvivaConnect = new TouchAction(driver);
		TouchAction tapDone = new TouchAction(driver);
		
		//Tap on "Allow" in notification pop-up		
		   new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(By.id("Allow")));
		   driver.findElementByAccessibilityId("Allow").click();
		  Thread.sleep(3000);
		
		  //tap the Login button 
			tapLoginButton.tap(200, 612).perform();
		    Thread.sleep(3000);
	
		    //tap on Email field and write in it, then switch to Password field and write in it
		    tapEmailField.tap(187, 250).perform();
			   driver.getKeyboard().pressKey(EMAIL);
			   Thread.sleep(2000);
			   driver.hideKeyboard();
			   driver.getKeyboard().pressKey(PASSWORD);
			   Thread.sleep(2000);
			   
			   //Tap Login button
			   tapLogin.tap(187, 333).perform();
			   Thread.sleep(15000);
			 
			   //Tap on "Okay" button inside a pop-up window
			   tapOkay.tap(187, 380).perform();
			   Thread.sleep(2000);
			   
			   //Tap on Sync button
			  new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOfElementLocated(By.name("Sync")));
			   driver.findElementByAccessibilityId("Sync").click();
			   
			  //Tap Add Device button
			   new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOfElementLocated(By.name("Add Device")));
			   driver.findElementByAccessibilityId("Add Device").click();
			   Thread.sleep(12000);
			   
			   //Tap on "Meters" button to get a list of available meters
			   tapMeter.tap(100, 70).perform();
			   Thread.sleep(2000);
			   
			
			   //Select "Aviva Connect" meter
			   tapAvivaConnect.tap(100, 120).perform();
			   Thread.sleep(2000);
			   
			   //Click on "Done" button after adding device
			   tapDone.tap(100, 650).perform();
			   Thread.sleep(2000);
			   
			   //Check if Aviva Connect meter is added
			   new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOfElementLocated(By.name("ACCU-CHEK Aviva Connect PAIR")));
			   driver.findElementByAccessibilityId("ACCU-CHEK Aviva Connect PAIR");	
			   Thread.sleep(2000);
	
	}
	



}
